#!/usr/bin/env python
from flask import Flask, make_response 
import matplotlib.pyplot as plt, mpld3
import numpy as np
app = Flask(__name__)

@app.route("/")
def simple():
    
    fig, ax = plt.subplots()
    x= np.random.rand(100)
    ax.hist(x)
    html_text = mpld3.fig_to_html(fig)
    return html_text


if __name__ == "__main__":
    app.run(debug=True, port=5000)
